require 'erb'
require 'pdfkit'

template = ARGV[0] || (puts("Usage: ruby #{__FILE__} <template_path>"); exit(1))
artifacts_dir = 'artifacts'
Dir.mkdir(artifacts_dir) unless Dir.exists?(artifacts_dir)
html_outfile = File.join(artifacts_dir, template.chomp('.erb'))
pdf_outfile = html_outfile.sub(".html", ".pdf")
erb = ERB.new(File.read(template))
@contact = {
  name: 'Isaac Post',
  phone: '330 690 6572',
  email: 'post.isaac@gmail.com',
  github: 'https://github.com/ipost',
  website: 'https://isaacpost.memery.org',
  location: 'Columbus, OH',
}
@experience = [
  {
    employer: 'CoverMyMeds',
    positions: [
      {
        title: 'Software Engineer',
        start_date: 'February 2017',
        end_date: 'current',
      },
    ],
    description: "This placeholder text is gonna be HUGE. I think the only difference between me and the other placeholder text is that I’m more honest and my words are more beautiful.\nSome people have an ability to write placeholder text... It's an art you're basically born with.\nYou either have it or you don't.\nI'm speaking with myself, number one, because I have a very good brain and I've said a lot of things.",
  },
  {
    employer: 'Nationwide Mutual Insurance Company',
    positions: [
      {
        title: 'Test Automation Developer',
        start_date: 'August 2015',
        end_date: 'February 2017',
      },
      {
        title: 'IT Test Analyst Intern',
        start_date: 'May 2017',
        end_date: 'August 2015',
      }
    ],
    description: "Created automated tests for web applications using Watir/Selenium.\nMaintained and optimized existing regression tests.\nCreated tools to support manual testing efforts.",
  },
  {
    employer: 'CoverMyMeds',
    positions: [
      {
        title: 'Software Engineer Co-Op',
        start_date: 'May 2014',
        end_date: 'December 2014',
      },
    ],
    description: 'Designed, implemented, tested, and maintained Rails and PHP web applications.',
  },
]
@skills = ['Ruby', 'Rails', 'Javascript', 'Linux', 'Git', 'Rust', 'Cucumber',
           'JMeter', 'Jenkins'] #sort by familiarity???
@projects = [
  {
    name: 'Halite 2',
    description: "Look at these words. Are they small words?\nAnd he referred to my words - if they're small, something else must be small.\nI guarantee you there's no problem, I guarantee.\nLorem Ipsum is FAKE TEXT!",
  },
]
html = erb.result
File.write(html_outfile, html)
puts "#{File.expand_path(html_outfile)}"
pdf = PDFKit.new(html, page_size: 'letter').to_pdf
File.write(pdf_outfile, pdf)
puts "#{File.expand_path(pdf_outfile)}"
